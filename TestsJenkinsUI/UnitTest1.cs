using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using Xunit;

namespace TestsJenkinsUI
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var capabilities = new DesiredCapabilities("chrome", "latest", new Platform(PlatformType.Any));
			capabilities.SetCapability("enableVNC", true);
			
            var driver = new RemoteWebDriver(new Uri("http://selenoid_selenoid_1:4444/wd/hub/"), capabilities);

            driver.Navigate().GoToUrl("https://www.google.com");

            driver.FindElement(By.XPath("//input[@class = 'gLFyf gsfi']"));

        }
    }
}
